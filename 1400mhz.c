#include <stdio.h>
#include <C:\Keil\C51\INC\ADI\aduc841.h>
#define ADI_EE_READ_PAGE   1
#define ADI_EE_WRITE_PAGE  2
#define ADI_EE_ERASE_PAGE  5
#define ADI_EE_ERASE_ALL   6

#define ADI_EEMEM_SIZE     640UL
void wait(void);
void Initialize();
void SPIWrite(unsigned char);
void flash_write(unsigned char reg);
unsigned char flash_read();
unsigned char flag;

sbit CE = P2^2; // PLL CHIP Enable o/p
sbit LE = P2^3;  // PLL Latch Enable o/p
sbit LD = P2^1;  // PLL Lock Detect i/p
sbit PDBRF = P2^4;  // PLL RF PowerDown Bit o/p
sbit LED = P2^0;  // LED o/p

//void wds_isr(void) interrupt
//{
//	LED = 0;
//	WDCON = 0x00;
//}

//1.30GHz settings
//unsigned int R0[]={0x00,0x20,0x80,0x00};
//unsigned int R1[]={0x08,0x00,0x86,0x41};
//unsigned int R2[]={0x02,0x00,0x4E,0x42};
//unsigned int R3[]={0x00,0x00,0x04,0xb3};
//unsigned int R4[]={0x00,0x1a,0x00,0x3c};
//unsigned int R5[]={0x00,0x58,0x00,0x05};


//1.4GHz settings
// unsigned char R0[]={0x00,0xd6,0x00,0x00};
// unsigned char R1[]={0x08,0x00,0x80,0x11};
// unsigned char R2[]={0x00,0x40,0x04,0xb3};
// unsigned char R3[]={0x00,0xE4,0x85,0xF3};
// unsigned char R4[]={0x00,0xa5,0x00,0x3c};
// unsigned char R5[]={0x00,0x58,0x00,0x05};

//1.07GHz settings
//unsigned char R0[]={0x00,0xd6,0x00,0x00};
//unsigned char R1[]={0x08,0x00,0x83,0x21};
//unsigned char R2[]={0x00,0x00,0x5F,0x42};
//unsigned char R3[]={0x00,0x00,0x04,0xb3};
//unsigned char R4[]={0x00,0xA5,0x00,0x3c};
//unsigned char R5[]={0x00,0x58,0x00,0x05};

 unsigned char R0[]={0x00,0xc0,0x00,0x00};
 unsigned char R1[]={0x08,0x00,0x80,0x11};
 unsigned char R2[]={0x00,0x00,0x4F,0x42};
 unsigned char R3[]={0x00,0x40,0x04,0xB3};
 unsigned char R4[]={0x00,0xa5,0x00,0x24};
 unsigned char R5[]={0x00,0x58,0x00,0x05};
 
void wait()
{
	int d;
	for(d=0;d <10;d++);
}
int i;

int main()
{
	Initialize();
	if(WDS == 1){
		WDCON = 0x01;
		WDCON = 0x30;
		PLLCON = 0x80;
		Initialize();
		//PCON = 0x22; //power down
	}
	else {
		WDCON = 0x01;
		WDCON = 0x72;
		Initialize();
	}

//	flash_write(0x55);
//	flag = flash_read();
//	 if(flag == 0x55){
//		 WDCON = 0x01;
//		 WDCON = 0x30;
//		 flash_write(0xFF);
//		 PLLCON = 0x80;	
//	 }

  
	while(1) {
			if (LD==1)
			{
				LED=1;
				
			}
			else
			{
				LED=0;
			}
	}

}
void SPIWrite(unsigned char reg){
	SPIDAT = reg;
	while(!ISPI);
	ISPI = 0;
}
void Initialize(){
		PDBRF = 1;
		SPICON=0x33;  // CPOL=0, CPHA=0, Fosc/16
		EA=1;			 // Enable all Gloabal Interrupt
		CE=1;				// Chip Enable PLL
		LD=1;

		for (i = 0; i<1024; i++)
		{
			LED = 1;
			wait();
			LED = 0;
			wait();
		}
		LE=0;
			for(i=0;i<=3;i++)
			{ 
				SPIWrite(R5[i]); 
			}
		LE=1;

		LE=0;
			for(i=0;i<=3;i++)
			{ 
				SPIWrite(R4[i]); 
			}
		LE=1;

		LE=0;
			for(i=0;i<=3;i++)
			{ 
				SPIWrite(R3[i]); 
			}
		LE=1;

		LE=0;
			for(i=0;i<=3;i++)
			{ 
				SPIWrite(R2[i]); 
			}
		LE=1;

		LE=0;
			for(i=0;i<=3;i++)
			{ 
				SPIWrite(R1[i]); 
			}
		LE=1;

		LE=0;
		  for(i=0;i<=3;i++)
			{ 
				SPIWrite(R0[i]); 
			}
		LE=1;
		
		
			
}	

